<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Resources\ProductCollection;

class ProductController extends Controller
{
    public function index()
    {
        $product   = new Product;
        $product   = $product->orderby('serie')->get();
        return $product ;
    }
}
